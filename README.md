Dep tree of `openapi-spec-validator`:
```
openapi-spec-validator==0.2.9
  jsonschema==3.2.0
    attrs==20.3.0
    pyrsistent==0.17.3
    setuptools==51.1.2
    six==1.15.0
  PyYAML==5.3.1
  six==1.15.0

```
The module stores oapi specs as a json schema and uses `jsonschema` to parse them.

### install.sh
Installs openapi-spec-validator with -t flag under `./unpacked/`. 

Removes yaml dependancies.    
Removes setuptools dependancy.    
Packs dependencies into `openapi_spec_validator.egg` (some package isn't happy with .zip)    

```
$ du -h openapi_spec_validator.egg
 
680K	openapi_spec_validator.egg
```
### Usage
The validator will look for references relative to the file path passed as argument. So relative pathing works. Refs are internally resolved to the absolute path *URI* ("file://absolute/path/to/spec"). 
```bash
python validate.py <target> 
```

```bash
python validate.py # uses default spec path from script 
```

```bash
python validate.py ./examples/basic.yaml # exits normally
python validate.py ./examples/swagger_v2.yaml # v2 schema, broken
python validate.py ./examples/link-example.yam # very broken yaml & schema
python validate.py ./examples/<amenities api spec> # ref works, breaks because errors.yaml is invalid(empty)
```
### Troubleshooting
If zipping manually make sure to zip the contents of `unpacked` and not the folder itself.



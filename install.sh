# install deps
pip install -t ./unpacked openapi-spec-validator
cd unpacked
# remove unneeded packages
rm -rf ./PyYAML*
rm -rf ./*yaml*
rm -rf ./setuptools*
rm -rf ./__pycache__
rm -rf ./pvectorc.cpython* # Should automatically switch to python api

zip -r9 ../openapi_spec_validator.egg *



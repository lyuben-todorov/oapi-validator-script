import sys
import os

try:
    from openapi_spec_validator import validate_spec
    from openapi_spec_validator.loaders import ExtendedSafeLoader
except ModuleNotFoundError:
    import pathlib
    p = pathlib.Path(__file__).parent.absolute()
    sys.path.insert(0, os.path.join(p, "openapi_spec_validator.egg"))
    from openapi_spec_validator import validate_spec
    from openapi_spec_validator.loaders import ExtendedSafeLoader
try:
    import yaml
except ImportError:
    import pathlib
    p = pathlib.Path(__file__).parent.absolute()
    sys.path.insert(0, os.path.join(p, "yaml.zip"))
    import yaml

spec_rel_path = './default.yaml'
if len(sys.argv) > 1:
    spec_rel_path = str(sys.argv[1])

def load_yaml(path):
    try:
        with open(path) as fh:
            return yaml.load(fh, Loader = ExtendedSafeLoader)
    except IOError as err:
        raise SystemExit(err)

spec_yaml = load_yaml(spec_rel_path)
abs_path = 'file://' + os.path.abspath(spec_rel_path)

validate_spec(spec_yaml, spec_url = abs_path)
